class NoSuchFileError extends Error {
  constructor(message = 'File with that name does not exist!') {
    super(message);
    this.statusCode = 400;
  }
}

class FileAlreadyExistsError extends Error {
  constructor(message = 'This file already exists!') {
    super(message);
    this.statusCode = 400;
  }
}

class BadPasswordError extends Error {
  constructor(message = 'Bad password!') {
    super(message);
    this.statusCode = 403;
  }
}

class InvalidInputError extends Error {
  constructor(message = 'Something wrong with your input!') {
    super(message);
    this.statusCode = 400;
  }
}

module.exports = {
  NoSuchFileError,
  FileAlreadyExistsError,
  BadPasswordError,
  InvalidInputError
};
