const fs = require('fs/promises');
const { NoSuchFileError, FileAlreadyExistsError } = require('./Errors');
const PasswordManager = require('./PasswordManager');
const passMan = new PasswordManager('passwd');

class FileWorker {
  constructor(rootDir) {
    this.rootDir = rootDir;

    (async () => {
      try {
        await fs.mkdir(rootDir, { recursive: true });
      } catch (error) {
        console.log(error);
      }
    })();
  }

  fileDoesNotExist = async fileName => {
    if (!(await this.listFiles()).includes(fileName)) {
      throw new NoSuchFileError();
    }
  };

  fileAlreadyExists = async fileName => {
    if ((await this.listFiles()).includes(fileName)) {
      throw new FileAlreadyExistsError();
    }
  };

  listFiles = async () => await fs.readdir(this.rootDir);

  returnContent = async fileName => {
    await this.fileDoesNotExist(fileName);
    const fileArr = fileName.split('.');
    const extension = fileArr[fileArr.length - 1];

    return {
      filename: fileName,
      content: await fs.readFile(`${this.rootDir}/${fileName}`, {
        encoding: 'utf-8'
      }),
      extension,
      uploadedDate: (await fs.stat(`${this.rootDir}/${fileName}`)).mtime
    };
  };

  updateFile = async fileObj => {
    const { filename, content } = fileObj;
    await this.fileDoesNotExist(filename);
    await fs.writeFile(`${this.rootDir}/${filename}`, content);
  };

  writeFile = async fileObj => {
    const { filename, content, password } = fileObj;
    await this.fileAlreadyExists(filename);
    await fs.writeFile(`${this.rootDir}/${filename}`, content);
    if (password) await passMan.addFileWithPass(filename, password);
  };

  deleteFile = async fileName => {
    await this.fileDoesNotExist(fileName);
    await fs.rm(`${this.rootDir}/${fileName}`);
    await passMan.rmFileWithPass(fileName);
  };
}

module.exports = FileWorker;
