const Joi = require('joi');

const validateFileObj = fileObj =>
  Joi.object()
    .keys({
      filename: Joi.string()
        .pattern(/^.+\.(log|txt|json|yaml|xml|js)$/i)
        .required(),
      content: Joi.string().min(1).required(),
      password: Joi.string().pattern(/^\S+$/m)
    })
    .validateAsync(fileObj);

const validateContentObj = async contentObj =>
  Joi.object()
    .keys({
      content: Joi.string().min(1).required()
    })
    .validateAsync(contentObj);

module.exports = { validateFileObj, validateContentObj };
