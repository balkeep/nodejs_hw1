const fs = require('fs/promises');

class PasswordManager {
  constructor(pswdFile) {
    this.pswdFile = pswdFile;

    (async () => {
      try {
        await fs.writeFile(pswdFile, JSON.stringify([]), { flag: 'wx' });
      } catch (e) {}
    })();
  }

  getDB = async () =>
    JSON.parse(await fs.readFile(this.pswdFile, { encoding: 'utf-8' }));

  getPassForFile = async filename => {
    const pswDB = await this.getDB();
    const fileEntry = pswDB.find(passEntry => passEntry.name === filename);
    return fileEntry ? fileEntry.pass : '';
  };

  addFileWithPass = async (name, pass) => {
    const pswDB = await this.getDB();
    if (!pswDB.find(passEntry => passEntry.name === name)) {
      pswDB.push({ name, pass });
      await fs.writeFile(this.pswdFile, JSON.stringify(pswDB));
    }
  };

  rmFileWithPass = async name => {
    const pswDB = await this.getDB();
    const pswDBFiltered = pswDB.filter(passEntry => passEntry.name !== name);
    await fs.writeFile(this.pswdFile, JSON.stringify(pswDBFiltered));
  };
}

module.exports = PasswordManager;
