const {
  NoSuchFileError,
  FileAlreadyExistsError,
  BadPasswordError,
  InvalidInputError
} = require('./Errors');

const errorHandler = (e, req, res, next) => {
  if (
    e instanceof NoSuchFileError ||
    e instanceof FileAlreadyExistsError ||
    e instanceof BadPasswordError ||
    e instanceof InvalidInputError
  ) {
    res.status(e.statusCode).json({ message: e.message });
  } else {
    res.status(500).json({ message: e.message });
  }
};

module.exports = errorHandler;
