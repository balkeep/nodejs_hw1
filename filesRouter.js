const express = require('express');
const router = express.Router();
const { validateFile, validateContent } = require('./validatorMiddleware');
const passMiddleware = require('./passMiddleware');
const FileWorker = require('./FileWorker');
const fileWorker = new FileWorker('./filesStorage');

router.get('/', async (req, res, next) => {
  try {
    const files = await fileWorker.listFiles();
    return res.status(200).json({ message: 'Success', files });
  } catch (e) {
    next(e);
  }
});

router.get('/:filename', passMiddleware, async (req, res, next) => {
  try {
    const file = await fileWorker.returnContent(req.params.filename);
    return res.status(200).json({ message: 'Success', ...file });
  } catch (e) {
    next(e);
  }
});

router.post('/', validateFile, async (req, res, next) => {
  try {
    await fileWorker.writeFile(req.body);
    return res.status(200).json({ message: 'File created successfully' });
  } catch (e) {
    next(e);
  }
});

router.put(
  '/:filename',
  passMiddleware,
  validateContent,
  async (req, res, next) => {
    try {
      await fileWorker.updateFile({
        filename: req.params.filename,
        content: req.body.content
      });
      return res.status(200).json({ message: 'File modified successfully' });
    } catch (e) {
      next(e);
    }
  }
);

router.delete('/:filename', passMiddleware, async (req, res, next) => {
  try {
    await fileWorker.deleteFile(req.params.filename);
    return res.status(200).json({ message: 'File deleted successfully' });
  } catch (e) {
    next(e);
  }
});

module.exports = router;
