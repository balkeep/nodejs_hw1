const morgan = require('morgan');
const express = require('express');
const app = express();

const errorHandler = require('./errorHandler');
const filesRouter = require('./filesRouter');

app.use(morgan('dev'));
app.use(express.json());
app.use('/api/files', filesRouter);
app.use(errorHandler);

app.listen(8080);
