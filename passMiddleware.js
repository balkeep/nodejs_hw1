const PasswordManager = require('./PasswordManager');
const passMan = new PasswordManager('passwd');
const { BadPasswordError } = require('./Errors');

const passMiddleware = async (req, res, next) => {
  const filename = req.params.filename;
  const password = req.query.password || '';

  const passInDB = await passMan.getPassForFile(filename);
  if (passInDB && password !== passInDB) {
    next(new BadPasswordError());
  }

  next();
};

module.exports = passMiddleware;
