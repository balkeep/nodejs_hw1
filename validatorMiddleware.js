const { validateFileObj, validateContentObj } = require('./InputValidator');
const { InvalidInputError } = require('./Errors');

const retrieveMsg = e => e.details[0]?.message.replaceAll('"', "'");

const validateFile = async (req, res, next) => {
  try {
    await validateFileObj(req.body);
  } catch (e) {
    next(new InvalidInputError(retrieveMsg(e)));
  }
  next();
};

const validateContent = async (req, res, next) => {
  try {
    await validateContentObj(req.body);
  } catch (e) {
    next(new InvalidInputError(retrieveMsg(e)));
  }
  next();
};

module.exports = { validateFile, validateContent };
